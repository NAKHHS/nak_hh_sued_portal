export default [
  {
    build: '01.03.2020 - 14:35:00',
    releaseNotes: {
      new: [
        'Suche nach beliebigen Werten über alle Spalten',
        'Ein- und ausblenden bestimmter Spalten',
        'Aufsteigende / Absteigende Sortierung bestimmter Spalten',
        'Mobilmodus: Auf kleinen Bildschirmen werden die Termine in einer Kartenansicht dargestellt'
      ],
      updated: [],
      fixed: []
    }
  },
  {
    build: '21.09.2019 - 10:15:17',
    releaseNotes: {
      new: [
        'Ansicht der Termine in Tabellenform',
        'Terminkategorien farblich hinterlegt'
      ],
      updated: [],
      fixed: []
    }
  }
]
