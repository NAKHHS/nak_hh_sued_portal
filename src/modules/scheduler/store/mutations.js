export function setMessage (state, Message) {
  state.Message = Message
}
export function setAppointments (state, Appointments) {
  state.Appointments = Appointments
}
export function flushAppointments (state) {
  state.Appointments = []
}
export function setAppointment (state, Appointment) {
  state.Appointments.push(Appointment)
}
export function updateAppointment (state, Appointment) {
  state.Appointments[Appointment.index] = {}
  state.Appointments[Appointment.index] = Appointment
}
export function setReleaseNotes (state, ReleaseNotes) {
  state.ReleaseNotes = ReleaseNotes
}
export function setMode (state, Mode) {
  state.Mode = Mode
}
export function setSelectedAppointment (state, Appointment) {
  state.SelectedAppointment = Appointment
}
export function setSelectedID (state, ID) {
  state.SelectedID = ID
}
export function changeDeletionMode (state, Data) {
  state.Appointments[Data.Index].inDeletionMode = Data.Status
}
export function deleteAppointment (state, Index) {
  state.Appointments[Index].isDeleted = true
}
export function removeAppointment (state, Index) {
  state.Appointments.splice(Index, 1)
}
export function Visibility (state, Data) {
  switch (Data.Dialog) {
    case 'EditorData':
      state.Dialogs.EditorDataVisible = Data.Value
      break
    case 'Form':
      state.Dialogs.FormVisible = Data.Value
  }
}
