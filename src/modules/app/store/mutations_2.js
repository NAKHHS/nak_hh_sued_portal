export function manuallySetUser (state, Values) {
  state.User.Firstname = Values.Firstname
  state.User.Lastname = Values.Lastname
  state.User.Email = Values.Email
}

export function addToConfiguration (state, Values) {
  state.Configuration = Values
}
