export default {
  Message: '',
  Appointments: [],
  ReleaseNotes: {
    Data: [],
    LSKey: 'portal_rlNotesScheduler'
  },
  Mode: '',
  SelectedAppointment: {},
  SelectedID: -1,
  SavedData: {},
  Dialogs: {
    EditorDataVisible: false,
    FormVisible: false
  }
}
