export default [
  {
    name: '',
    version: {
      major: 0,
      minor: 0,
      patch: 1,
      alpha: -1,
      beta: -1,
      build: 190921101517
    },
    releaseType: 'development', // orDevelopment, Alpha, Beta, RC, Release
    releaseNotes: {
      new: ['Ansicht der Termine in Tabellenform', 'Terminkategorien farblich hinterlegt'],
      updated: [],
      fixed: []
    }
  }
]
