
const routes = [
  {
    path: '/',
    component: () => import('layouts/Portal.vue'),
    children: [
      { path: '', component: () => import('./../modules/app/pages/Index.vue') },
      {
        path: 'Termine',
        component: () => import('./../modules/scheduler/pages/Index.vue')
      },
      {
        path: 'GB',
        component: () => import('./../modules/gb/pages/Index.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('./../modules/app/pages/Error404.vue')
  }
]

export default routes
