const routes = [
  {
    path: '/',
    component: () => import('./../modules/app/layouts/Portal.vue'),
    children: [
      { path: '', component: () => import('./../modules/app/pages/Index.vue') },
      {
        path: 'Termine',
        component: () => import('./../modules/scheduler/pages/Index.vue')
      },
      {
        path: 'GB',
        component: () => import('./../modules/gb/pages/Index.vue')
      },
      {
        path: 'Diakone',
        component: () => import('./../modules/diakone/pages/Index.vue')
      },
      {
        path: 'Statistiken',
        component: () => import('./../modules/statistics/pages/Index.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('./../modules/app/pages/Error404.vue')
  })
}

export default routes
