/* eslint-disable space-before-function-paren */
export default {
  testFunktion() {},
  getURL(Vue) {
    return Vue.$store.state.App.Configuration.api.url
  },
  async getSync(Vue, Request) {
    const lobjReturn = await Vue.$axios.post(this.getURL(Vue) + 'get', {
      request: Request
    })
    return lobjReturn
  },
  async controllerSync(Vue, Controller, Method, Parameter) {
    if (Parameter !== undefined) {
      const lobjRequest = { values: Parameter }
      const lobjReturn = await Vue.$axios.post(
        this.getURL(Vue) + Controller + '\\' + Method,
        {
          request: lobjRequest
        }
      )
      return lobjReturn
    } else {
      const lobjReturn = await Vue.$axios.post(
        this.cURL(Vue) + Controller + '\\' + Method,
        {}
      )
      return lobjReturn
    }
  }
}
